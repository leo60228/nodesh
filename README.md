# `nodesh`
Based on https://github.com/TehShrike/shell-tag, nodesh is similar except it can be piped using [`@babel/plugin-proposal-pipeline-operator`](https://babeljs.io/docs/en/next/babel-plugin-proposal-pipeline-operator):

```js
const sh = require('.');

console.log(sh`echo ls \\| echo hi:`);
console.log(sh`ls` |> sh`echo hi`);
console.log(sh`echo ls \\| cat:`);
console.log(sh`ls` |> sh`cat`);
console.log(sh`echo ls \\| sed 's/\\./_dot_/g':`);
console.log(sh`ls` |> sh`sed 's/\\./_dot_/g'`);
```

prints:

```
ls | echo hi:

hi

ls | cat:

example.js
index.js
node_modules
package.json
README.md

ls | sed s/\./_dot_/g:

example_dot_js
index_dot_js
node_modules
package_dot_json
README_dot_md
```

## How does it work?
By modifying a function to that when it is converted to a string it executes the command, but when it is executed directly it creates a new object of this type. This new object's command is changed to being `${argument} | ${originalCommand}`. This means that the `|>` operator is changed to being `|` inside the command.

## Why?
This lets you use more JS constructs when writing "shell"-scripts using this method. This is useful because if you stored piped functions as plain strings, they would be escaped improperly when used.

## Caveats
Commands are not actually executed until the output is processed by `util.inspect`, or have their `.toString` or `.exec` methods executed. This is necessary so that it acts as function.
